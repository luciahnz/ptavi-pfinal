"""Cliente."""
# !/usr/bin/python3
# -- coding: utf-8 --
import socket
import secrets
import os
import random
from xml.sax import make_parser
import time
import sys
import xml.etree.ElementTree as ET
import simplertp
from xml.sax.handler import ContentHandler
import hashlib

tiempo = time.mktime((time.gmtime()[0],
                      time.gmtime()[1],
                      time.gmtime()[2], 0, 0, 0, 0, 0, 0))
t = time.time()
dif = t - tiempo
dif = int(dif)
fecha = time.strftime('%Y-%m-%d', time.gmtime(t))


class leerxml(ContentHandler):
    """Clase."""

    def __init__(self):
        """Clase."""
        self.DIC = []

    def startElement(self, name, attrs):
        """Clase."""
        if name == 'account':
            self.mainname = attrs.get('username', "")
            self.password = attrs.get('passwd', "")
            self.usseraccount = {"etiqueta": "account",
                                 'username': self.mainname,
                                 'passwd': self.password}
            self.DIC.append([name, self.usseraccount])

        elif name == 'uaserver':
            self.dir_ip = attrs.get('ip', "")
            self.port = attrs.get('puerto', "")
            self.server = {"etiqueta": "uaserver",
                           'ip': self.dir_ip,
                           'puerto': self.port}
            self.DIC.append([name, self.server])

        elif name == 'rtpaudio':
            self.port = attrs.get('puerto', "")
            self.audio = {"etiqueta": "rtpaudio",
                          'puerto': self.port}
            self.DIC.append([name, self.audio])

        elif name == 'regproxy':
            self.dir_ip = attrs.get('ip', "")
            self.port = attrs.get('puerto', "")
            self.proxy = {"etiqueta": "regproxy",
                          'ip': self.dir_ip,
                          'puerto': self.port}
            self.DIC.append([name, self.proxy])

    def get_tags(self):
        """Clase."""
        return (self.DIC)


def Calclog(log, modo, dir_ip, port, contexto):  # modo será sent o received
    """Reloj."""
    doc = open(log, "a")
    contexto = contexto.replace('\r\n', " ")
    contenido = str(fecha) + ' ' + str(dif)\
        + ' ' + modo + ' ' + dir_ip + ':' + str(port) + ' ' + contexto
    doc.write(contenido + "\n")
    doc.close()


if __name__ == "__main__":

    parser = make_parser()
    cHandler = leerxml()
    parser.setContentHandler(cHandler)
    parser.parse(open(str(sys.argv[1])))
    usuario = cHandler.get_tags()[0][1]['username']
    contraseña = cHandler.get_tags()[0][1]['passwd']
    dir_ipservi = cHandler.get_tags()[1][1]['ip']
    puerto_servi = cHandler.get_tags()[1][1]['puerto']
    proxy_rtp = cHandler.get_tags()[2][1]['puerto']
    dir_ipp = cHandler.get_tags()[3][1]['ip']
    puertop = cHandler.get_tags()[3][1]['puerto']
    acceso = ET.parse(str(sys.argv[1]))
    for w in acceso.getroot().findall('log'):
        log = w.text
    for w in acceso.getroot().findall('audio'):  # Element.findall
        audioe = w.text
    AUDIOPATH = audioe

    if str(sys.argv[2]) == 'REGISTER':
        llamada = ('REGISTER sip:' + usuario + ':' + puerto_servi +
                   ' SIP/2.0\r\n' + 'Expires: ' + str(sys.argv[3])
                   + '\r\n\r\n')
        print('Envio a Proxy: ' + llamada + '\r\n')
        Calclog(log, 'Sent to ', dir_ipp, str(puertop), llamada)

    elif str(sys.argv[2]) == 'INVITE':
        tp1 = str(sys.argv[2]) + ' sip:' + str(sys.argv[3]) + ' SIP/2.0\r\n'
        tp2 = tp1 + 'DIC-Type: application/sdp\r\n'
        tp = tp2 + 'DIC-Length: '
        llamando1 = "v=0\r\n" + "o=" + usuario + ' ' + dir_ipservi + "\r\n"
        llamando2 = llamando1 + "s=misesion\r\n" + 't=0\r\n'
        llamando3 = llamando2 + "m=audio " + proxy_rtp + " RTP"
        llamada = tp + str(len(bytes(llamando3, 'utf-8')))
        llamada += "\r\n\r\n" + "v=0\r\n"
        print('Envio a Proxy: ' + llamada + '\r\n')
        Calclog(log, 'Sent to ', dir_ipp, str(puertop), llamada)
    elif str(sys.argv[2]) == 'BYE':
        llamada = str(sys.argv[2]) \
                  + ' sip:' + str(sys.argv[3]) + ' SIP/2.0\r\n'
        print('Envio a Proxy' + llamada + '\r\n')
        Calclog(log, 'Sent to ', dir_ipp, str(puertop), llamada)
    if str(sys.argv[2]) != 'BYE' and str(sys.argv[2]) \
            != 'INVITE' and str(sys.argv[2]) != 'REGISTER':
        print('Usage: python uaclient.py config method option')

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((dir_ipp, int(puertop)))
        my_socket.send(bytes(llamada, 'utf-8'))
        # recibiendo del proxy
        leo = my_socket.recv(1024)
        print('Obtenido ', leo.decode('utf-8'))
        Calclog(log, 'Received from ', dir_ipp,
                str(puertop), leo.decode('utf-8'))

        if leo.decode('utf-8').startswith('SIP/2.0 401 Unauthorized\r\n'):

            hashlib.sha256().update(bytes(leo.decode('utf-8').split('"')[1],
                                          'utf-8'))
            hashlib.sha256().update(bytes(contraseña, 'utf-8'))
            dar = str(sys.argv[2]) + ' sip:' + usuario + ':' + puerto_servi \
                + ' SIP/2.0\r\n' + 'Expires: ' + str(sys.argv[3]) + '\r\n\r\n'
            ok = dar + "Authorization: Digest response= " + '"' \
                + hashlib.sha256().hexdigest() + '"\r\n'
            Calclog(log, 'Sent to ', dir_ipp, str(puertop), ok)
            my_socket.send(bytes(ok, 'utf-8') + b'\r\n')

        if str(sys.argv[2]) == 'INVITE':
            envio = "SIP/2.0 100 Trying\r\n"
            envio += "SIP/2.0 180 Ringing\r\n "
            envio += "SIP/2.0 200 OK\r\n"
            if envio in leo.decode('utf-8'):
                my_socket.send(bytes('ACK sip:' + str(sys.argv[3])
                                     + ' SIP/2.0\r\n', 'utf-8'))
                Calclog(log, 'Sent to ', dir_ipp, str(puertop),
                        'ACK sip:' + str(sys.argv[3]) + ' SIP/2.0\r\n')
                RTP_Header = simplertp.RtpHeader()
                RTP_Header.set_header(version=2, marker=secrets.randbits(1),
                                      payload_type=14,
                                      ssrc=random.randint(0, 10000000))
                simplertp.send_rtp_packet(RTP_Header,
                                          simplertp.RtpPayloadMp3(AUDIOPATH),
                                          dir_ipservi, int(proxy_rtp))
                my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                my_socket.connect(('127.0.0.1', int('5456')))
                my_socket.send(bytes('ACK sip:' + str(sys.argv[3])
                                     + ' SIP/2.0\r\n', 'utf-8'))
            print("Cliente finalizado")
