
"""Servidor."""
# !/usr/bin/python3
# -- coding: utf-8 --

import socketserver
import secrets
import os
import random
from xml.sax import make_parser
import time
import sys
import xml.etree.ElementTree as ET
import simplertp
from xml.sax.handler import ContentHandler
import hashlib


tiempo = time.mktime((time.gmtime()[0],
                      time.gmtime()[1],
                      time.gmtime()[2], 0, 0, 0, 0, 0, 0))
t = time.time()
dif = t - tiempo
dif = int(dif)
fecha = time.strftime('%Y-%m-%d',
                      time.gmtime(t))


class sleerxml(ContentHandler):
    """Clase."""

    def __init__(self):
        """Clase."""
        self.DIC = []

    def startElement(self, name, attrs):
        """Clase."""
        if name == 'account':
            self.mainname = attrs.get('username', "")
            self.password = attrs.get('passwd', "")
            self.usseraccount = {"etiqueta": "account",
                                 'username': self.mainname,
                                 'passwd': self.password}
            self.DIC.append([name, self.usseraccount])

        elif name == 'uaserver':
            self.dir_ip = attrs.get('ip', "")
            self.port = attrs.get('puerto', "")
            self.server = {"etiqueta": "uaserver",
                           'ip': self.dir_ip,
                           'puerto': self.port}
            self.DIC.append([name, self.server])

        elif name == 'rtpaudio':
            self.port = attrs.get('puerto', "")
            self.audio = {"etiqueta": "rtpaudio",
                          'puerto': self.port}
            self.DIC.append([name, self.audio])

        elif name == 'regproxy':
            self.dir_ip = attrs.get('ip', "")
            self.port = attrs.get('puerto', "")
            self.proxy = {"etiqueta": "regproxy",
                          'ip': self.dir_ip,
                          'puerto': self.port}
            self.DIC.append([name, self.proxy])

    def get_tags(self):
        """Clase."""
        return (self.DIC)


def Calclog(log, modo, dir_ip, puerta, contexto):
    """Clase."""
    doc = open(log, "a")
    contexto = contexto.replace('\r\n', " ")
    contenido = str(fecha) + ' ' + str(dif)\
        + ' ' + modo + ' ' + dir_ip + ':' + str(puerta) + ' ' + contexto
    doc.write(contenido + "\n")
    doc.close()


class EchoHandler(socketserver.DatagramRequestHandler):
    """Clase."""

    guardado = {}

    def handle(self):
        """Clase."""

        while 1:

            if not self.rfile.read():
                break
            if self.rfile.read().decode('utf-8').split(' ')[0] == "INVITE":

                llamando = "\r\n\r\n" + "v=0\r\n"
                llamando += "o=" + usuario + ' ' + dir_ipservi + "\r\n"
                llamando += "s=misesion\r\n" + 't=0\r\n'
                llamando += "m=audio " + proxy_rtp + " RTP"

                esperando = 'SIP/2.0 100 Trying\r\n\r\n' \
                            'SIP/2.0 180 Ringing\r\n\r\n' \
                            'SIP/2.0 200 OK\r\n'
                esperando += 'DIC-Type: application/sdp\r\n'
                esperando += 'DIC-Length: ' + str(len(llamando))
                Calclog(log, 'Received from ', dir_ipp,
                        str(puertop), self.rfile.read().decode('utf-8'))

                self.wfile.write(str.encode(esperando + llamando))

            elif self.rfile.read().decode('utf-8').split(' ')[0] == "BYE":
                Calclog(log, 'Received from ', dir_ipp, str(puertop),
                        llamando.decode('utf-8'))
                self.wfile.write(bytes('SIP/2.0 200 OK\r\n\r\n', 'utf-8'))

            elif self.rfile.read().decode('utf-8').split(' ')[0] == "ACK":
                Calclog(log, 'Received from ', dir_ipp,
                        str(puertop), llamando.decode('utf-8'))

                simplertp.RtpHeader().set_header(version=2,
                                                 marker=secrets.randbits(1),
                                                 payload_type=14,
                                                 ssrc=random.randint(0, 1000))
                simplertp.send_rtp_packet(simplertp.RtpHeader(),
                                          simplertp.RtpPayloadMp3(lp),
                                          dir_ipservi, int(rtpaudio_p))
                Calclog(log, 'Sent to ', dir_ipp, str(puertop),
                        llamando.decode('utf-8'))

            elif self.rfile.read().decode('utf-8').split(' ')[0] \
                    != 'ACK' and\
                    self.rfile.read().decode('utf-8').split(' ')[0] \
                    != 'REGISTER' \
                    and self.rfile.read().decode('utf-8').split(' ')[0] \
                    != 'INVITE':
                self.wfile.write(bytes('SIP/2.0 405 Method Not Allowed\r\n\r',
                                       'utf-8'))

            else:
                self.wfile.write(bytes('SIP/2.0 400 Bad Request\n\r\n',
                                       'utf-8'))
                Calclog(log, 'Sent to ', dir_ipp, str(puertop),
                        'SIP/2.0 400 Bad Request\n\r\n')


if __name__ == "__main__":

    parser = make_parser()
    cHandler = sleerxml()
    parser.setContentHandler(cHandler)
    parser.parse(open(str(sys.argv[1])))
    print(cHandler.get_tags())
    usuario = cHandler.get_tags()[0][1]['username']
    contraseña = cHandler.get_tags()[0][1]['passwd']
    dir_ipservi = cHandler.get_tags()[1][1]['ip']
    puerto_servi = cHandler.get_tags()[1][1]['puerto']
    proxy_rtp = cHandler.get_tags()[2][1]['puerto']
    dir_ipp = cHandler.get_tags()[3][1]['ip']
    puertop = cHandler.get_tags()[3][1]['puerto']
    acceso = ET.parse(str(sys.argv[1]))
    for w in acceso.getroot().findall('log'):
        log = w.text
    for w in acceso.getroot().findall('audio'):
        lp = w.text
    escucha = 'listening...'
    print("Lanzando server UDP de eco...")
    Calclog(log, 'Iniciando ', dir_ipp, str(puertop), escucha)

    print(puerto_servi)

    serv = socketserver.UDPServer((dir_ipservi, int(puerto_servi)),
                                  EchoHandler)

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print('Servidor desconectado')
