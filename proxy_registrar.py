"""Proxy."""
# !/usr/bin/python3
# -- coding: utf-8 --
import sys
import json
from uaclient import leerxml
import time
import socketserver
import socket
import hashlib
from xml.sax import make_parser
import secrets
from xml.sax.handler import ContentHandler


tiempo = time.mktime((time.gmtime()[0],
                      time.gmtime()[1],
                      time.gmtime()[2], 0, 0, 0, 0, 0, 0))
t = time.time()
dif = t - tiempo
dif = int(dif)
fecha = time.strftime('%Y-%m-%d',
                      time.gmtime(t))


class pleerxml(ContentHandler):
    """Clase."""

    def __init__(self):
        """Clase."""
        self.DIC = []

    def startElement(self, name, attrs):
        """Clase."""
        if name == 'server':
            self.port = attrs.get('puerto', "")
            self.username = attrs.get('name', "")
            self.dir_ip = attrs.get('ip', "")
            self.servi = {"etiqueta": "server",
                          'puerto': self.port,
                          'name': self.username,
                          'ip': self.dir_ip}
            self.DIC.append([name, self.servi])

        elif name == 'database':
            self.path = attrs.get('path', "")
            self.password = attrs.get('passwdpath', "")
            self.d_base = {"etiqueta": "database",
                           'path': self.path,
                           'passwdpath': self.password}
            self.DIC.append([name, self.d_base])

        elif name == 'log':
            self.path = attrs.get('path', "")
            self.log = {"etiqueta": "log",
                        'path': self.path}
            self.DIC.append([name, self.log])

    def get_tags(self):
        """Clase."""
        return (self.DIC)


def Calclog(modo, dir_ip, puerta, contexto):
    """Clase."""
    doc = open(logpath, "a")
    contexto = contexto.replace('\r\n', " ")
    contenido = str(fecha) + ' ' + str(dif) + ' ' + modo \
        + ' ' + dir_ip + ':' + str(puerta) + ' ' + contexto
    doc.write(contenido + "\n")
    doc.close()


class ProxyEchoHandler(socketserver.DatagramRequestHandler):
    """Clase."""

    registradorr = {}
    contraseññas = {}
    guard = {}

    def jsonpasswd(self):
        """Clase."""
        try:
            with open(contraseña_db, 'r') as jsonfile:
                self.contraseññas = json.load(jsonfile)
        except FileNotFoundError:
            print("Except")
            pass

    def register2json(self):
        """Clase."""
        with open('registro.json', 'w') as file:
            json.dump(self.registradorr, file, indent=2)

    def handle(self):
        """Clase."""
        leo = self.rfile.read()
        text = leo.decode('utf-8') + '\r\nVia: SIP/2.0/UDP' + ip_servidor + ':'
        text += puerto_servidor + '\r\n'
        print('Llamando ' + leo.decode('utf-8'))
        self.register2json()
        self.jsonpasswd()

        if leo.decode('utf-8').split(' ')[0] == 'REGISTER':
            print('Usuario:',
                  leo.decode('utf-8').split()[1].split(':')[1].split(':')[-1])
            print('Expira en:',
                  int(leo.decode('utf-8').split('\r\n')[1].split()[1]))
            puerta = leo.decode('utf-8').split()[1].split(':')[2]
            juja = leo.decode('utf-8').split()[1].split(':')[1].split(':')[-1]
            ñu = leo.decode('utf-8').split('\r\n')[1].split()[1]
            shrek = leo.decode('utf-8').split()[1].split('sip:')[1]
            asno = leo.decode('utf-8').split(' ')[0]
            if leo.decode('utf-8').split()[1].split('sip:')[1] \
                    in self.registradorr:
                if int(leo.decode('utf-8').split('\r\n')[1].split()[1]) == 0:
                    print(leo.decode('utf-8').split()[1].split('sip:')[1] +
                          ' ya no esta registrado\r')
                    k = leo.decode('utf-8').split()[1].split('sip:')[1]
                    del self.registradorr[k]
                    self.wfile.write(b'SIP/2.0 200 OK\r\n')
                if int(leo.decode('utf-8').split('\r\n')[1].split()[1]) != 0:

                    print('usuario: ' +
                          juja +
                          ' se ha registrado correctamente\r')
                    self.wfile.write(b'SIP/2.0 200 OK\r\n')

            if int(leo.decode('utf-8').split('\r\n')[1].split()[1]) > 0:

                if leo.decode('utf-8').split()[1].split('sip:')[1] \
                        not in self.registradorr:
                    largo = leo.decode('utf-8').split()[1].split('sip:')[1]
                    self.registradorr[largo] \
                        = [self.client_address[0],
                           int(puerta),
                           time.time(),
                           int(ñu)]
                    self.jsonpasswd()
                    print(self.registradorr)

                    if 'Authorization: Digest response= ' \
                            in leo.decode('utf-8'):
                        lu = leo.decode('utf-8').split()[1].split('sip:')[1]
                        self.jsonpasswd()
                        hashlib.sha256().update(bytes(self.guard[lu] +
                                                      contraseña_db, 'utf-8'))
                        hashlib.sha256().update(bytes(contraseña_db, 'utf-8'))
                        hashlib.sha256().digest()
                        hashlib.sha256().hexdigest()

                        long = leo.decode('utf-8').split()[8]
                        if str(hashlib.sha256().hexdigest()) \
                                == long.split('"')[1]:
                            pa = decode('utf-8').split()[1].split('sip:')[1]
                            pe = split('\r\n')[1].split()[1]
                            self.registradorr[leo.pa] \
                                = [self.client_address[0],
                                   int(puerta),
                                   time.time(),
                                   int(leo.decode('utf-8').pe)]

                            self.register2json()
                            self.wfile.write(bytes('SIP/2.0 200 OK\r\n',
                                                   'utf-8')
                                             + b'\r\n')

                        else:
                            ka = split()[1].split('sip:')[1]
                            self.guard[leo.decode('utf-8').ka] \
                                = str(secrets.randbits(8))

                            leo = 'SIP/2.0 401 Unauthorized\r\n'
                            leo += 'WWW Autenticate: Digest nonce= '
                            leo += '"' + str(secrets.randbits(8)) \
                                   + '"' + '\r\n\r\n'

                            self.wfile.write(bytes(leo, 'utf-8')
                                             + b'\r\n')

                    else:
                        self.guard[shrek] \
                            = secrets.token_hex(15).upper()
                        leo = 'SIP/2.0 401 Unauthorized\r\n'
                        leo += 'WWW Autenticate: Digest nonce= '
                        leo += '"' + secrets.token_hex(15).upper() +\
                               '"' + '\r\n\r\n'
                        self.wfile.write(bytes(leo, 'utf-8')
                                         + b'\r\n')

        if asno == 'INVITE':

            print(leo.decode('utf-8').split()[1].split(':')[1].split(':')[-1])
            if leo.decode('utf-8').split()[1].split(':')[1].split(':')[-1] \
                    in self.registradorr:
                with open('registro.json', 'r') as file:
                    libro = json.load(file)
                    save = list(libro.save())
                    evalu = list(libro.evalu())

                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) \
                        as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((sentto, int(save[1].split(':')[1])))
                    my_socket.send(bytes(mensaje, 'utf-8'))
                    llave = my_socket.recv(1024)
                    Calclog('Received from ', sentto, save[1].split(':')[1],
                            llave.decode('utf-8'))
                    self.wfile.write(llave)
                    Calclog('Sent to ',
                            self.client_address[0],
                            str(self.client_address[1]), text)
                    envio = b"SIP/2.0 100 Trying\r\n"
                    envio += b"SIP/2.0 180 Ringing\r\n "
                    envio += b"SIP/2.0 200 OK\r\n"
                    self.wfile.write(envio)

            if leo.decode('utf-8').split()[1].split(':')[1].split(':')[-1] \
                    not in self.registradorr:
                wa = split()[1].split(':')[1].split(':')[-1]
                print(leo.decode('utf-8').wa +
                      'no encontrado\r')

                self.wfile.write(b'SIP/2.0 404 User Not Found\r\n')

        if asno == 'BYE':
            with open('registro.json', 'r') as file:
                libro = json.load(file)
                save = list(libro.save())
                evalu = list(libro.evalu())
                sentto = evalu[1][0]

            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                    as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET,
                                     socket.SO_REUSEADDR, 1)
                my_socket.connect((sentto, int(save[1].split(':')[1])))
                my_socket.send(bytes(text, 'utf-8'))
                llave = my_socket.recv(1024)
                Calclog('Received from ', sentto, save[1].split(':')[1],
                        llave.decode('utf-8'))
                self.wfile.write(llave)
                Calclog('Sent to ',
                        self.client_address[0],
                        str(self.client_address[1]),
                        leo.decode('utf-8'))
                self.wfile.write(b"SIP/2.0 200 OK\r\n")

        if asno == 'ACK':
            with open('registro.json', 'r') as file:
                libro = json.load(file)
                save = list(libro.save())
                evalu = list(libro.evalu())
                sentto = evalu[1][0]

            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                    as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET,
                                     socket.SO_REUSEADDR, 1)
                my_socket.connect((evalu[1][0], int(save[1].split(':')[1])))
                my_socket.send(bytes(text, 'utf-8'))
                llave = my_socket.recv(1024)
                self.wfile.write(llave)
                Calclog('Sent to ',
                        self.client_address[0],
                        str(self.client_address[1]),
                        leo.decode('utf-8'))
                self.wfile.write(b"ACK SIP/2.0\r\n")

        if asno != 'BYE' \
                and asno != 'INVITE' \
                and asno != 'REGISTER':
            self.wfile.write(bytes('SIP/2.0 405 Method Not Allowed\r\n\r\n',
                                   'utf-8'))
            Calclog('Sent to ',
                    evalu[1][0],
                    str(save[1].split(':')[1]),
                    'SIP/2.0 405 Method Not Allowed\r\n\r\n')


if __name__ == "__main__":

    try:

        if len(sys.argv) != 2:
            sys.exit("Usage: python3 proxy_registrar.py config ")

        else:
            parser = make_parser()
            cHandler = pleerxml()
            parser.setContentHandler(cHandler)
            parser.parse(open(str(sys.argv[1])))

    except FileNotFoundError:
        sys.exit("Usage: python3 proxy_registrar.py config ")

    try:
        nombre = cHandler.get_tags()[0][1]['name']
        ip_servidor = cHandler.get_tags()[0][1]['ip']
        puerto_servidor = cHandler.get_tags()[0][1]['puerto']
        contraseña_db = cHandler.get_tags()[1][1]['passwdpath']
        logpath = cHandler.get_tags()[2][1]['path']
    except (IndexError, ValueError):
        sys.exit("Usage: python3 proxy_registrar.py config ")

    print('Iniciando servidor ' + nombre +
          puerto_servidor + '...\r\n\r\n')

    try:
        socketserver.UDPServer((ip_servidor, int(puerto_servidor)),
                               ProxyEchoHandler).serve_forever()

    except KeyboardInterrupt:
        print('Proxy desconectado')
